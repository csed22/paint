package eg.edu.alexu.csd.oop.draw;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ShapeHelper {

	@SuppressWarnings({ "unchecked", "resource" })
	public static Class<? extends Shape> openJar(String jarPath) {

		JarFile jarFile = null;
		try {
			jarFile = new JarFile(jarPath);
		} catch (IOException e2) {
			return null;
		}

		Enumeration<JarEntry> e = jarFile.entries();

		URLClassLoader cl = null;

		try {
			URL[] urls = { new URL("jar:file:" + jarPath + "!/") };
			cl = URLClassLoader.newInstance(urls);
		} catch (MalformedURLException e1) {
		}

		while (e.hasMoreElements()) {
			JarEntry je = e.nextElement();
			if (je.isDirectory() || !je.getName().endsWith(".class")) {
				continue;
			}

			// -6 because of .class
			String className = je.getName().substring(0, je.getName().length() - 6);
			className = className.replace('/', '.');
			Class<?> c = null;
			try {
				c = cl.loadClass(className);
			} catch (ClassNotFoundException e1) {
			}

			try {
				if (c.newInstance() instanceof Shape) {
					return (Class<? extends Shape>) c;
				}
			} catch (InstantiationException | IllegalAccessException e1) {
			}
		}

		return null;
	} // https://stackoverflow.com/questions/11016092/how-to-load-classes-at-runtime-from-a-folder-or-jar

	public static String checkClass(Class<? extends Shape> shapeClass) {
		return shapeClass == null ? null : getLastName(shapeClass.getName());
	}

	public static String checkShape(Shape shape) {
		return shape == null ? null : getLastName(shape.getClass().getName());
	}

	public static Shape instantiateShape(String s, List<Class<? extends Shape>> shapeClass) {

		for (int i = 0; i < shapeClass.size(); i++)
			if (s.equals(getLastName(shapeClass.get(i).getName())))
				try {
					return shapeClass.get(i).newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					System.out.println("instantiateShape failed!");
				}

		return null;
	}

	private static String getLastName(String s) {

		int lastDot = 0;

		for (int i = 0; i < s.length(); i++)
			if (s.charAt(i) == '.')
				lastDot = i;

		String lastName = new String();

		for (int i = lastDot + 1; i < s.length(); i++)
			lastName += s.charAt(i);

		return lastName;

	}

}

//  https://stackoverflow.com/questions/541749/how-to-determine-an-objects-class