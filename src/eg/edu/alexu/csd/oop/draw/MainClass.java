package eg.edu.alexu.csd.oop.draw;

import javax.swing.*;

@SuppressWarnings("serial")
public class MainClass extends JFrame{

    private static Pane pane;

    private MainClass(){
        setResizable(false);
        setVisible(true);
        setBounds(0,0,800,600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

	public static void main(String[] args){
    	MainClass window = new MainClass();
        window.setLocationRelativeTo(null);
        pane = new Pane();
        window.add(pane);
    }

}

