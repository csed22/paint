package eg.edu.alexu.csd.oop.draw.shapes;

import java.awt.Graphics;
import java.util.HashMap;
import java.util.Map;

import eg.edu.alexu.csd.oop.draw.Shape;

public class Square extends Rectangle  {
	
	public Square() {
		super();
	}
	
	public void draw(Graphics canvas) {
			
		Double width  = properties.get("Width") .doubleValue();
		properties.put("Length" , width);
		super.draw(canvas);
		
	} 
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object clone() throws CloneNotSupportedException{
		Shape toCopy = new Square();
		toCopy.setPosition(upperLeft);
		toCopy.setColor(color);
		toCopy.setFillColor(fillColor);

        Map copyProp = new HashMap();
        for (final Map.Entry key : properties.entrySet()) {
        	copyProp.put(key.getKey(), key.getValue());
        }
        toCopy.setProperties(copyProp);
        
        return toCopy;
	}

}