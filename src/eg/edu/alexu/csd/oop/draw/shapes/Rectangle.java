package eg.edu.alexu.csd.oop.draw.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

import eg.edu.alexu.csd.oop.draw.Shape;

public class Rectangle implements Shape{

	protected Point upperLeft;
	protected HashMap<String, Double> properties;
	protected Color color;
	protected Color fillColor;
	
	public Rectangle() {
		properties = new HashMap<String, Double>();
		properties.put("Width" , 0.0 );
		properties.put("Length", 0.0 );
	}
	
	public void setPosition(Point position) {
		upperLeft = position;
	}

	public Point getPosition() {
		return upperLeft;
	}

	public void setProperties(Map<String, Double> properties) {
		this.properties = (HashMap<String, Double>) properties;
	}
 
	public Map<String, Double> getProperties() {
		return properties;
	}

	public void setColor(Color color) {
		this.color= color;
	}

	public Color getColor() {
		return color;
	}

	public void setFillColor(Color color) {
		this.fillColor = color;
	}

	public Color getFillColor() {
		return fillColor;
	}

	public void draw(Graphics canvas) {
		
		int width  = properties.get("Width") .intValue();
		int length = properties.get("Length").intValue(); 
		
		Graphics2D gg = ((Graphics2D)canvas);
		gg.setColor(fillColor);
		gg.fillRect(upperLeft.x, upperLeft.y, Math.abs(width)  , Math.abs(length));
		gg.setColor(color);
		gg.drawRect(upperLeft.x, upperLeft.y, Math.abs(width)  , Math.abs(length));
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object clone() throws CloneNotSupportedException{
		Shape toCopy = new Rectangle();
		toCopy.setPosition(upperLeft);
		toCopy.setColor(color);
		toCopy.setFillColor(fillColor);

        Map copyProp = new HashMap();
        for (final Map.Entry key : properties.entrySet()) {
        	copyProp.put(key.getKey(), key.getValue());
        }
        toCopy.setProperties(copyProp);
        
        return toCopy;
	}
	
} 