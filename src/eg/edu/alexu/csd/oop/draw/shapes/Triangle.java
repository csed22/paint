package eg.edu.alexu.csd.oop.draw.shapes;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.util.HashMap;
import java.util.Map;

import eg.edu.alexu.csd.oop.draw.Shape;

public class Triangle extends Rectangle {

	public Triangle() {
		super();
	}
	
	public void draw(Graphics canvas) {

		Double width  = properties.get("Width") .doubleValue();
		Double length = properties.get("Length").doubleValue();
		
		width  = Math.abs(width);
		length = Math.abs(length);
		
		int[] x_coordinates = { upperLeft.x, (int) (upperLeft.x + Math.round(width)), (int) (upperLeft.x + Math.round(width)/2) };
		int[] y_coordinates = { (int) (upperLeft.y + Math.round(length)), (int) (upperLeft.y + Math.round(length)), upperLeft.y };
		
		Polygon poly = new Polygon( x_coordinates, y_coordinates , 3);

		Graphics2D gg = ((Graphics2D)canvas);
		gg.setColor(color);
		gg.drawPolygon(poly);
		gg.setColor(fillColor);
		gg.fillPolygon(poly);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object clone() throws CloneNotSupportedException{
		Shape toCopy = new Triangle();
		toCopy.setPosition(upperLeft);
		toCopy.setColor(color);
		toCopy.setFillColor(fillColor);

        Map copyProp = new HashMap();
        for (final Map.Entry key : properties.entrySet()) {
        	copyProp.put(key.getKey(), key.getValue());
        }
        toCopy.setProperties(copyProp);
        
        return toCopy;
	}
	
}