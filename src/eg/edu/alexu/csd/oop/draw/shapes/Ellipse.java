package eg.edu.alexu.csd.oop.draw.shapes;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.HashMap;
import java.util.Map;

import eg.edu.alexu.csd.oop.draw.Shape;

public class Ellipse extends Rectangle {
	
	public Ellipse() {
		super();
	}
	
	public void draw(Graphics canvas) {
		
		int width  = properties.get("Width") .intValue();
		int length = properties.get("Length").intValue(); 

		Graphics2D gg = ((Graphics2D)canvas);
		gg.setColor(fillColor);
		gg.fillOval(upperLeft.x, upperLeft.y, Math.abs(width)  , Math.abs(length));
		gg.setColor(color);
		gg.drawOval(upperLeft.x, upperLeft.y, Math.abs(width)  , Math.abs(length));
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object clone() throws CloneNotSupportedException{
		Shape toCopy = new Ellipse();
		toCopy.setPosition(upperLeft);
		toCopy.setColor(color);
		toCopy.setFillColor(fillColor);

        Map copyProp = new HashMap();
        for (final Map.Entry key : properties.entrySet()) {
        	copyProp.put(key.getKey(), key.getValue());
        }
        toCopy.setProperties(copyProp);
        
        return toCopy;
	}
	
}