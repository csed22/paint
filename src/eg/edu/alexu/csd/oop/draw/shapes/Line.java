package eg.edu.alexu.csd.oop.draw.shapes;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

import eg.edu.alexu.csd.oop.draw.Shape;

public class Line extends Rectangle {

	public Line() {
		super();
	}  
	  
	public void draw(Graphics canvas) {
		
		int width  = properties.get("Width") .intValue();
		int length = properties.get("Length").intValue(); 
		
		Point endP = new Point(( upperLeft.x + width ),( upperLeft.y + length ));
		fillColor = color;
		
		Graphics2D gg = ((Graphics2D)canvas);
		gg.setColor(color);
		gg.drawLine(upperLeft.x, upperLeft.y, endP.x, endP.y);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object clone() throws CloneNotSupportedException{
		Shape toCopy = new Line();
		toCopy.setPosition(upperLeft);
		toCopy.setColor(color);
		toCopy.setFillColor(fillColor);

        Map copyProp = new HashMap();
        for (final Map.Entry key : properties.entrySet()) {
        	copyProp.put(key.getKey(), key.getValue());
        }
        toCopy.setProperties(copyProp);
        
        return toCopy;
	}
	
}