package eg.edu.alexu.csd.oop.draw;

import java.awt.Graphics;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import eg.edu.alexu.csd.oop.draw.shapes.Circle;
import eg.edu.alexu.csd.oop.draw.shapes.Ellipse;
import eg.edu.alexu.csd.oop.draw.shapes.Line;
import eg.edu.alexu.csd.oop.draw.shapes.Rectangle;
import eg.edu.alexu.csd.oop.draw.shapes.Square;
import eg.edu.alexu.csd.oop.draw.shapes.Triangle;

public class Paint implements DrawingEngine {

	private Shape[] shapesArray;
	private ArrayList<Shape> shapesHolder;
	private ArrayList<HistoryStep> history;
	private List<Class<? extends Shape>> SupportedShapes;
	private ArrayList<HistoryStep> redoContainer;
	public boolean enableUndoRedoCap = true;

	// Constructor
	public Paint() {
		shapesHolder  = new ArrayList<Shape>(); 
		history 	  = new ArrayList<HistoryStep>();
		redoContainer = new ArrayList<HistoryStep>();
		
		SupportedShapes = new ArrayList<Class<? extends Shape>>();
		
		SupportedShapes.add(Rectangle.class);
		SupportedShapes.add(Square	 .class);
		SupportedShapes.add(Ellipse	 .class);
		SupportedShapes.add(Circle	 .class);
		SupportedShapes.add(Triangle .class);
		SupportedShapes.add(Line	 .class);
		}
	
	// Easylife: will not be used in main program.
	public void clean() {
		shapesHolder.clear();
		history.clear();
		redoContainer.clear();
	}
	
	// Helping Methods
	private void dynamicToStaticArray() { 
		shapesArray = new Shape[shapesHolder.size()];
		
		for(int i=0; i< shapesHolder.size(); i++) 
			shapesArray[i]=shapesHolder.get(i);	
	}
	
	private void performHistoryStep(HistoryStep log) {
		
			if(log.getCommand().equalsIgnoreCase("Add")) {
				shapesHolder.add(log.getShape()); 
			}
	   else if(log.getCommand().equalsIgnoreCase("Remove")) {
				shapesHolder.remove(log.getShape());
	   		}
	   else if(log.getCommand().equalsIgnoreCase("Update")) {
				shapesHolder.remove(log.getShape());
				shapesHolder.add(log.getUpdatedShape());
  		}
		
	}
	
	private HistoryStep invertHistoryStep(HistoryStep log) {
		
		if(log.getCommand().equalsIgnoreCase("Remove")) {
			log.setCommand("Add");
		}
   else if(log.getCommand().equalsIgnoreCase("Add")) {
	   		log.setCommand("Remove");
   		}
   else if(log.getCommand().equalsIgnoreCase("Update")) {
	   		Shape tmpShape = log.getUpdatedShape();
	   		log.setUpdatedShape(log.getShape());
	   		log.setShape(tmpShape);
		}
		
		return log;
		
	}
	
	@SuppressWarnings("unused")
	private void printHistory() {
		System.out.println("History :");
		for(int i=0; i<history.size(); i++)
			System.out.println(history.get(i));
		
		System.out.println("redoContainer :");
		for(int i=0; i<redoContainer.size(); i++)
			System.out.println(redoContainer.get(i));
	}

	
	private void checkHistoryandRedoCap() {
		if(enableUndoRedoCap) {
			if(history.size() > 20) {
				history.remove(0);
			}
			if(redoContainer.size() > 20) {
				redoContainer.remove(0);
			}
		}
	}
	

	public void refresh(Graphics canvas) {  
		for(int i=0; i< shapesHolder.size(); i++) {
			shapesHolder.get(i).draw(canvas);
		}		
	}

	public void addShape(Shape shape) {	
		shapesHolder.add(shape);
		history.add(new HistoryStep("Add",shape,null));
		redoContainer.clear();
		checkHistoryandRedoCap();
	}

	public void removeShape(Shape shape) {
		if(shapesHolder.size() > 0) {
			shapesHolder.remove(shape);
			history.add(new HistoryStep("Remove",shape,null));
			checkHistoryandRedoCap();
		} 
	}

	public void updateShape(Shape oldShape, Shape newShape) {
		if(shapesHolder.size() > 0) shapesHolder.remove(oldShape);
		shapesHolder.add(newShape);
		history.add(new HistoryStep("Update",oldShape,newShape));
		checkHistoryandRedoCap();
	}
	@Override
	public Shape[] getShapes() {
		dynamicToStaticArray();
		return shapesArray;
	}

	public void undo() {
		if(history.size()>0) {
			HistoryStep whatIsDone 	 = history.get(history.size()-1);
			HistoryStep toBeInverted = invertHistoryStep(whatIsDone);
			performHistoryStep(toBeInverted);							// do inverted operation
			history.remove(whatIsDone); 
			whatIsDone = invertHistoryStep(toBeInverted);				// invert the inverted = original, because they are attached to the same object
			redoContainer.add(whatIsDone);
		}
	}

	public void redo() {
		if(redoContainer.size()>0) {
			HistoryStep toBeDone = redoContainer.get(redoContainer.size()-1);
			performHistoryStep(toBeDone);
			history.add(toBeDone);
			redoContainer.remove(toBeDone);
		}
	}

	public List<Class<? extends Shape>> getSupportedShapes() {
		installPluginShape(".\\RoundRectangle.jar");
		return SupportedShapes;
	}

	public void installPluginShape(String jarPath) {
		
		Class<? extends Shape> c = ShapeHelper.openJar(jarPath);
		
		boolean duplicate = false;
		
		for(int i=0; i<SupportedShapes.size(); i++) {
			if(c != null)
				if(ShapeHelper.checkClass(c).equals(ShapeHelper.checkClass(SupportedShapes.get(i))))
					duplicate = true;
		}
		if(c != null)
			if(!duplicate)
				SupportedShapes.add((Class<? extends Shape>) c);
		
	}


	public void save(String path) {		
		if(path!=null) {
			FileOutputStream fout; 
			try {
				fout = new FileOutputStream(new File(path));
				XMLEncoder encoder = new XMLEncoder(fout);
	        	 encoder.writeObject(shapesHolder); 
	        	 encoder.close();
	        	 fout.close();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {		
				e1.printStackTrace();	
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void load(String path) {
		if(path!=null) {
			clean();
			FileInputStream fin;
			try {
				fin = new FileInputStream(new File(path));
				XMLDecoder encoder = new XMLDecoder(fin);
	        	shapesHolder =  (ArrayList<Shape>) encoder.readObject();
	        	encoder.close();
	        	fin.close();
			} catch (FileNotFoundException e1) {				
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

}
