package eg.edu.alexu.csd.oop.draw;

public class HistoryStep {
	
	private String command;
	private Shape shape;
	private Shape updatedShape;
	
	public HistoryStep(String command, Shape shape, Shape updatedShape){
		
		this.command = command;
		this.shape = shape;
		
		if(command.equals("Update"))
			this.updatedShape = updatedShape;
		else
			this.updatedShape = null;
		
	}
	
	public String toString() {
		String s = command;
		
		s+= ", ";
		s+= ShapeHelper.checkShape(shape);
	
		s+= ", ";
		s+= ShapeHelper.checkShape(updatedShape);
		
		return s;
	
	}

	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	
	public Shape getShape() {
		return shape;
	}
	public void setShape(Shape shape) {
		this.shape = shape;
	}
	
	public Shape getUpdatedShape() {
		return updatedShape;
	}
	public void setUpdatedShape(Shape updatedShape) {
		this.updatedShape = updatedShape;
	}

}
