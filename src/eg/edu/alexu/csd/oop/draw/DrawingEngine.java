package eg.edu.alexu.csd.oop.draw;

/**
 * Drawing and Painting Application, Dynamic, Save and load
 * Parts(2, 3, 4)
 * @author 9_64_paint
 */

public interface DrawingEngine {
	
	// Part(2): Drawing and Painting Application:				<-------->

	/**
	 * redraw all shapes on the canvas, might be quite useful
	 * consider using it in add, remove and update shape
	 * @param canvas
	 */
    public void refresh(java.awt.Graphics canvas);

    
    /**
     * add shape to the canvas
     * @param shape shape to be added
     */
    public void addShape(Shape shape);
    /**
     * remove shape from the canvas
     * @param shape shape to be removed
     */
    public void removeShape(Shape shape);
    /**
     * update current shape properties
     * @param oldShape current shape with unwanted properties
     * @param newShape new shape with modified properties
     */
    public void updateShape(Shape oldShape, Shape newShape);

    
    /**
     * getter for an array of all created shapes
     * @return the created shapes objects
     */
    public Shape[] getShapes();
    

    /**
     * undo these actions: addShape, removeShape, updateShape
     * consider a placeholder for at least 20 steps
     */
    public void undo();
    /**
     * redo these actions: addShape, removeShape, updateShape
     * consider a placeholder for at least 20 steps
     */
    public void redo();
    
    
	// Part(3): Dynamic Application Extensions and plug-ins:	<-------->
    
    /**
     * return the classes (types) of supported shapes already exist and the
     * ones that can be dynamically loaded at runtime
     * @return classes of supported shapes
     */
    public java.util.List<Class<? extends Shape>> getSupportedShapes();
    /**
     * add to the supported shapes the new shape class
     * @param jarPath
     */
    public void installPluginShape(String jarPath);

    
    // Part(4): Save and load:									<-------->
    
    /**
     * save the drawing in XML and JSON file, User must choose where 
     * to save the file and be able to modify the shapes
     * @param path file path
     */
    public void save(String path);
    /**
     * load previously saved drawings
     * @param path the saved file path
     * @throws Exception throw runtime exception when unexpected extension 
     */
    public void load(String path);
    
}