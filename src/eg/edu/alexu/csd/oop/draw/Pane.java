package eg.edu.alexu.csd.oop.draw;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import eg.edu.alexu.csd.oop.draw.shapes.Line;
import eg.edu.alexu.csd.oop.draw.shapes.Rectangle;

@SuppressWarnings("serial")
public class Pane extends JPanel implements ActionListener{
	
	private Point firstPoint;
	private Point secondPoint;
	
	private Timer refreshTimer;
	private DrawingEngine actor;
	
	private JLabel labelDSL;
	private JScrollPane scrollPaneSSL;
	private JScrollPane scrollPaneDSL;
	private JList<String> drawnShapesJlist;
	private JList<String> supportedShapesJList;

    private JButton deleteButton;
    private JButton undoButton;
    private JButton redoButton;
    private JButton moveButton; 
    private JButton resizeButton;
    private JButton borderColorB; 
    private JButton fillColorB;
    private JButton saveButton;
    private JButton loadButton;
       
    private Shape newShape;
    private Shape selectedShape;
    private Shape[] arrShapes;
    
    private String[] shapes;
    private List<Class<? extends Shape>> supportedShapes;
        
    public Pane(){
    	
    	initialVariables();
    	
        setBounds(0,0,800,600);
        setVisible(true);
        setLayout(null);

        MouseHandler handler = new MouseHandler();
        this.addMouseListener(handler);

        undoButton    = addButton(undoButton  ,"Undo"  , 0);
        redoButton    = addButton(redoButton  ,"Redo"  , 1);
        deleteButton  = addButton(deleteButton,"Delete", 2);
        
        moveButton    = addButton(moveButton  ,"Move"  , 3);
        resizeButton  = addButton(resizeButton,"Resize", 4);
        
        borderColorB  = addButton(borderColorB,"Color" , 5);
        fillColorB    = addButton(fillColorB  ,"Fill"  , 6);
       
        saveButton 	 = addButton(saveButton  ,"Save"   , 8);
        loadButton   = addButton(loadButton  ,"Load"   , 9);
        
        supportedShapesJList = new JList<String>(shapes);
		supportedShapesJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPaneSSL = new JScrollPane(supportedShapesJList);
		scrollPaneSSL.setBounds(670,15,100,130);
		add(scrollPaneSSL);
		supportedShapesJList.setSelectedIndex(0);
		
		labelDSL = new JLabel("Layers:");
		labelDSL.setBounds(670,160,100,20);
		add(labelDSL);
		
		drawnShapesJlist = new JList<String>(updateCurrentShapes(arrShapes));
		drawnShapesJlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		drawnShapesJlist.setVisibleRowCount(20);

		scrollPaneDSL = new JScrollPane(drawnShapesJlist);
		scrollPaneDSL.setBounds(670,180,100,365);
		add(scrollPaneDSL);

		reloading();
		
		supportedShapesJList.addListSelectionListener(
			new ListSelectionListener() {				
				public void valueChanged(ListSelectionEvent event) {
					if(supportedShapesJList.getSelectedIndex()>=0) {
						String s = shapes[supportedShapesJList.getSelectedIndex()];
						newShape = ShapeHelper.instantiateShape(s,supportedShapes);
					}
				}
			}
		);
		
		drawnShapesJlist.addListSelectionListener(
				new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent event) {
						if(arrShapes.length>0 && drawnShapesJlist.getSelectedIndex()>=0)
							selectedShape = arrShapes[drawnShapesJlist.getSelectedIndex()];
					}
				}
		);
    } 

    public void actionPerformed (ActionEvent e){
        Object source = e.getSource();
        Graphics g = this.getGraphics();
        
        if (source == deleteButton  && drawnShapesJlist.getSelectedIndex()>=0 )
	       	actor.removeShape(selectedShape);
        
        if (source == moveButton    && drawnShapesJlist.getSelectedIndex()>=0 ){
        	Shape movedshape = null ;
        	try {
				 movedshape =  (Shape) selectedShape.clone();
			} catch (CloneNotSupportedException e1) {}

        	Point moveToPoint = new Point();
        	
        	String Readchange = JOptionPane.showInputDialog("Enter new X");
			moveToPoint.x = Integer.parseInt(Readchange);
			
			Readchange = JOptionPane.showInputDialog("Enter new Y");
			moveToPoint.y = Integer.parseInt(Readchange);
			
			movedshape.setPosition(moveToPoint);
			actor.updateShape(selectedShape, movedshape);
        }
        if (source == resizeButton  && drawnShapesJlist.getSelectedIndex()>=0 ){
        	Shape resizedShape = null ;
        	try {
				 resizedShape =  (Shape) selectedShape.clone();
			} catch (CloneNotSupportedException e1) {}

			Map<String, Double> resizedShapeProb = resizedShape.getProperties();
			
			String Readchange = JOptionPane.showInputDialog("Enter new width");
			resizedShapeProb.put("Width", Double.parseDouble(Readchange)); 
			
			Readchange = JOptionPane.showInputDialog("Enter new length");
			resizedShapeProb.put("Length", Double.parseDouble(Readchange));
			
			resizedShape.setProperties(resizedShapeProb);
			actor.updateShape(selectedShape, resizedShape);
        }
        
        if ((source == borderColorB || source == fillColorB)  && drawnShapesJlist.getSelectedIndex()>=0 ){
        	Shape coloredshape = null ;
        	try {
        		coloredshape =  (Shape) selectedShape.clone();
			} catch (CloneNotSupportedException e1) {}
        	
        	if(source == borderColorB)
        		coloredshape.setColor(JColorChooser.showDialog(null, "Pick your color", selectedShape.getColor()));
        	else if (source == fillColorB)
        		coloredshape.setFillColor(JColorChooser.showDialog(null, "Pick your color", selectedShape.getFillColor()));
        	
        	actor.updateShape(selectedShape, coloredshape);
        }
        
        if (source == undoButton) {
        	actor.undo();
        }
        if (source == redoButton) {
        	actor.redo();
        }

        if(source == saveButton) {
            String path = JOptionPane.showInputDialog("write the path");
            actor.save(path);	
        } 
        if(source == loadButton) {
            String path = JOptionPane.showInputDialog("write the path");
            actor.load(path);
        }
        
        DoRefresh(g);
    }
    
	private void drawIt(){
		
	    	Point upperLeft   = new Point(Math.min(firstPoint.x, secondPoint.x),Math.min(firstPoint.y, secondPoint.y));
	    	Point lowwerRight = new Point(Math.max(firstPoint.x, secondPoint.x),Math.max(firstPoint.y, secondPoint.y));
	    	
	    	if( newShape instanceof Line ) {
	    		upperLeft   = firstPoint;
		    	lowwerRight = secondPoint;
	    	}
	    	
	    	newShape.setPosition(upperLeft);
	    	
	    	Map<String, Double> newShapeProp = newShape.getProperties();
	    	
	    	newShapeProp.put("Width" , (double) (lowwerRight.x - upperLeft.x));
	    	newShapeProp.put("Length", (double) (lowwerRight.y - upperLeft.y));
	    	
	    	newShape.setProperties(newShapeProp);
	    	
	    	newShape.setColor(Color.BLACK);
	    	newShape.setFillColor(Color.WHITE);
	    	
	    	try {
				actor.addShape((Shape)newShape.clone());
			} catch (CloneNotSupportedException e) {}
	    	
			arrShapes = actor.getShapes();
	        drawnShapesJlist.setListData(updateCurrentShapes(arrShapes));
	        
	        Graphics g = this.getGraphics();
	        DoRefresh(g);
    }
    
	private void reloading() {
		TimerTask reloading = new TimerTask() {
			
			public void run() {
				
				repaint();
				//doLayout();
				//revalidate();
				cancel();
				
			}	// https://stackoverflow.com/questions/12865803/how-do-i-refresh-a-gui-in-java
		};
		refreshTimer = new Timer();
		refreshTimer.scheduleAtFixedRate(reloading, 100, 100);
	}
	
    private void DoRefresh(Graphics g) {
        repaint();
        arrShapes = actor.getShapes();
        drawnShapesJlist.setListData(updateCurrentShapes(arrShapes));
        updateSupportShapes();
        supportedShapesJList.setListData(shapes);
        g = this.getGraphics();
        paintAgain(g);
    }
    
    private void paintAgain(Graphics g) {
    	TimerTask refresh = new TimerTask() {
			
			public void run() {
				
				actor.refresh(g);
				cancel();
				
			}	// https://stackoverflow.com/questions/1409116/how-to-stop-the-task-scheduled-in-java-util-timer-class
		};
		refreshTimer = new Timer();
		refreshTimer.scheduleAtFixedRate(refresh, 100, 100);
		
    }	// https://youtu.be/MjGnLRt6M6w
    
    private String[] updateCurrentShapes(Shape[] arrOfShapes) {
    	
    	String[] array = new String[arrOfShapes.length];
    	
    	for(int i = 0; i<arrOfShapes.length; i++)        	
    		array[i] = ShapeHelper.checkShape(arrOfShapes[i]) + " " + (i+1);
    	
		return array;
    }
    
    private JButton addButton(JButton button, String text, int order) {
    	button = new JButton(text);
    	button.setBounds(20, order*55 + 15,80,40);
    	button.addActionListener(this);
        add(button);
        return button;
    }
   
    private void initialVariables() {
    	
    	selectedShape =null;
        actor = new Paint();
        newShape = new Rectangle();
        arrShapes = actor.getShapes();
        
        updateSupportShapes();
        
    }
    
    private void updateSupportShapes() {
    	
    	supportedShapes = actor.getSupportedShapes();
        shapes = new String[supportedShapes.size()];
        
        for(int i=0; i<shapes.length; i++) {
        	shapes[i] = ShapeHelper.checkClass(supportedShapes.get(i));
        }
        
    }
    
    private class MouseHandler implements MouseListener{
    	
		public void mouseClicked(MouseEvent event) {
		}
		
		public void mousePressed(MouseEvent event) {
			firstPoint = new Point(event.getX(),event.getY());
		}
		
		public void mouseReleased(MouseEvent event) {
			secondPoint = new Point(event.getX(),event.getY());
			drawIt();
		}
		
		public void mouseEntered(MouseEvent event) {
		}
		
		public void mouseExited(MouseEvent event) {
		}

	}
    
}

// Thank You <3 -> https://stackoverflow.com/questions/29585353/paint-with-swing-java
// used:	    -> this.getGraphics();