package eg.edu.alexu.csd.oop.draw;

/**
 * Geometric Shapes Data Model
 * Part (1)
 * @author 9_64_paint
 */

public interface Shape{

	/**
	 * setter for the position of the shape
	 * @param position the center point (x, y) position
	 */
    public void setPosition(java.awt.Point position);
    /**
	 * getter for the position of the shape
	 * @return position of the center point (x, y)
	 */
    public java.awt.Point getPosition();

        
    /**
     * update shape specific properties (e.g., radius),it is recommended to
     * check a java.util.Map tutorial: https://youtu.be/MwPrMAG4bLg
     * @param properties map implementation of your choice to the properties
     */
    public void setProperties(java.util.Map<String, Double> properties);
    /**
     * getter for shape specific properties (e.g., radius)
     * @return properties map to the properties of the shape
     */
    public java.util.Map<String, Double> getProperties();

    
    /**
	 * setter for the border color of the shape, will use setProperties method
	 * g.setColor(Color.RED);		<-- border color
	 * g.drawRect(10, 55, 100, 30);
	 * g.setColor(Color.GREEN);
	 * g.fillRect(11, 56, 99, 29);
	 * @param color border color for the shape
	 */
    public void setColor(java.awt.Color color);
    /**
     * getter for the border color of the shape, will use getProperties method
     * @return color border color for the shape
     */
    public java.awt.Color getColor();

    
    /**
	 * setter for the color of the shape, will use setProperties method
	 * g.setColor(Color.RED);
	 * g.drawRect(10, 55, 100, 30);
	 * g.setColor(Color.GREEN);		<-- fill color
	 * g.fillRect(11, 56, 99, 29);
	 * @param color fill color for the shape
	 */
    public void setFillColor(java.awt.Color color);
    /**
     * getter for the color of the shape, will use getProperties method
     * @return color fill color for the shape
     */
    public java.awt.Color getFillColor();

    /**
     * redraw the shape on the canvas, might need JPanel and paintComponent
     * @param canvas 
     */
    public void draw(java.awt.Graphics canvas);

    
    /**
     * create a deep clone of the shape, override the default method
     * @return independent copy of the shape
     * @throws CloneNotSupportedException Object.clone() declares it
     */
    public Object clone() throws CloneNotSupportedException;
    
}